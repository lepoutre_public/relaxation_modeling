# relaxation_modeling

This repository **data** contains data file for the manuscript **Modeling relaxation experiments with a mechanistic model of gene expression**
Two types of data were collected on days 2, 5, 9, 13, 19, 23, 26 and 30 : cell counts and fluorescence distribution. The cell counts allow us to quantify proliferation whereas the fluorescence measure the distribution of CD34 expression. They were collected at CRCL by Sylvain Lefort, Véronique Maguer-Satta and Thibault Voeltzel

Two types of files are considered 
- .fcs file for fluorescence with label precising the day (in 2, 5, 9, 13, 19, 23, 26 and 30) and the intial seed (CD34- or CD34+ sorted cells)
- a xlsx file for cell counts at day 2, 5, 9, 13, 19, 23, 26 and 30
